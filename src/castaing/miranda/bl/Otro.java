package castaing.miranda.bl;

import java.time.LocalDate;

public class Otro extends Material {

    private String descripcion;

    public Otro() {
        super();
        descripcion = " ";
    }

    public Otro(String signatura, LocalDate fechaCompra, char restringido, String tema, String descripcion) {
        super(signatura, fechaCompra, restringido, tema);
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Otro{" +
                super.toString() +
                "descripcion='" + descripcion + '\'' +
                '}';
    }

}
