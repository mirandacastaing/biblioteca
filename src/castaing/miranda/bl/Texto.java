package castaing.miranda.bl;

import java.time.LocalDate;

public class Texto extends Material {

    private String titulo;
    private String autor;
    private LocalDate fechaPublicacion;
    private int paginas;
    private String idioma;

    public Texto() {
        super();
        titulo = " ";
        autor = " ";
        fechaPublicacion = LocalDate.now();
        idioma = " ";
    }

    public Texto(String signatura, LocalDate fechaCompra, char restringido, String tema, String titulo, String autor,
                 LocalDate fechaPublicacion, int paginas, String idioma) {
        super(signatura, fechaCompra, restringido, tema);
        this.titulo = titulo;
        this.autor = autor;
        this.fechaPublicacion = fechaPublicacion;
        this.paginas = paginas;
        this.idioma = idioma;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public LocalDate getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(LocalDate fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    @Override
    public String toString() {
        return "Texto{" +
                super.toString() +
                "titulo='" + titulo + '\'' +
                ", autor='" + autor + '\'' +
                ", fechaPublicacion=" + fechaPublicacion +
                ", paginas=" + paginas +
                ", idioma='" + idioma + '\'' +
                '}';
    }

}
