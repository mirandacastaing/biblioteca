package castaing.miranda.bl;

import java.time.LocalDate;

public class Material {

    protected String signatura;
    protected LocalDate fechaCompra;
    protected char restringido;
    protected String tema;

    public Material() {
        signatura = " ";
        fechaCompra = LocalDate.now();
        restringido = ' ';
        tema = " ";
    }

    public Material(String signatura, LocalDate fechaCompra, char restringido, String tema) {
        this.signatura = signatura;
        this.fechaCompra = fechaCompra;
        this.restringido = restringido;
        this.tema = tema;
    }

    public String getSignatura() {
        return signatura;
    }

    public void setSignatura(String signatura) {
        this.signatura = signatura;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public char getRestringido() {
        return restringido;
    }

    public void setRestringido(char restringido) {
        this.restringido = restringido;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    @Override
    public String toString() {
        return "Material{" +
                "signatura='" + signatura + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido=" + restringido +
                ", tema='" + tema + '\'' +
                '}';
    }

}
