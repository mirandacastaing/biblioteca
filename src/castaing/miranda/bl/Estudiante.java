package castaing.miranda.bl;

public class Estudiante extends Persona {

    private String carrera;
    private int creditos;

    public Estudiante() {
        super();
        carrera = " ";
    }

    public Estudiante(String nombre, String apellido, String id, String carrera, int creditos) {
        super(nombre, apellido, id);
        this.carrera = carrera;
        this.creditos = creditos;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    @Override
    public String toString() {
        return "La información del estudiante es " + super.toString().substring(32) + ", " + carrera + ", " + creditos;
    }

}
