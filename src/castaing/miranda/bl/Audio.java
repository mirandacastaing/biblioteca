package castaing.miranda.bl;

import java.time.LocalDate;

public class Audio extends Material {

    protected char formato;
    protected int duracion;
    protected String idioma;

    public Audio() {
        super();
        formato = ' ';
        idioma = " ";
    }

    public Audio(String signatura, LocalDate fechaCompra, char restringido, String tema, char formato, int duracion,
                 String idioma) {
        super(signatura, fechaCompra, restringido, tema);
        this.formato = formato;
        this.duracion = duracion;
        this.idioma = idioma;
    }

    public char getFormato() {
        return formato;
    }

    public void setFormato(char formato) {
        this.formato = formato;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    @Override
    public String toString() {
        return "Audio{" +
                super.toString() +
                "formato=" + formato +
                ", duracion=" + duracion +
                ", idioma='" + idioma + '\'' +
                '}';
    }

}
