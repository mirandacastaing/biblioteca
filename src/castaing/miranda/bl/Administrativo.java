package castaing.miranda.bl;

public class Administrativo extends Persona {

    private char nombramiento;
    private int horas;

    public Administrativo() {
        super();
        nombramiento = ' ';
    }

    public Administrativo(String nombre, String apellido, String id, char nombramiento, int horas) {
        super(nombre, apellido, id);
        this.nombramiento = nombramiento;
        this.horas = horas;
    }

    public char getNombramiento() {
        return nombramiento;
    }

    public void setNombramiento(char nombramiento) {
        this.nombramiento = nombramiento;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    @Override
    public String toString() {
        return "La información del administrativo es " + super.toString().substring(32) + ", " + nombramiento + ", " +
                horas;
    }

}
