package castaing.miranda.bl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Profesor extends Persona {

    private char contrato;
    private LocalDate fechaContratacion;

    public Profesor() {
        super();
        contrato = ' ';
        fechaContratacion = LocalDate.now();
    }

    public Profesor(String nombre, String apellido, String id, char contrato, String fechaContratacion) {
        super(nombre, apellido, id);
        this.contrato = contrato;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
        this.fechaContratacion = LocalDate.parse(fechaContratacion, formatter);
    }

    public char getContrato() {
        return contrato;
    }

    public void setContrato(char contrato) {
        this.contrato = contrato;
    }

    public LocalDate getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(LocalDate fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    @Override
    public String toString() {
        return "La información del profesor es " + super.toString().substring(32) + ", " + contrato + ", " +
                fechaContratacion;
    }

}
