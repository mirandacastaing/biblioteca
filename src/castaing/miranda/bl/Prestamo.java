package castaing.miranda.bl;

public class Prestamo {

    private Persona persona;
    private Material material;

    public Prestamo() {
    }

    public Prestamo(Persona persona, Material material) {
        this.persona = persona;
        this.material = material;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Prestamo{" +
                "persona=" + persona +
                ", material=" + material +
                '}';
    }

}
