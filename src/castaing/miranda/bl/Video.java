package castaing.miranda.bl;

import java.time.LocalDate;

public class Video extends Audio {

    private String director;

    public Video() {
        super();
        director = " ";
    }

    public Video(String signatura, LocalDate fechaCompra, char restringido, String tema, char formato, int duracion,
                 String idioma, String director) {
        super(signatura, fechaCompra, restringido, tema, formato, duracion, idioma);
        this.director = director;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return "Video{" +
                super.toString() +
                "director='" + director + '\'' +
                '}';
    }

}
