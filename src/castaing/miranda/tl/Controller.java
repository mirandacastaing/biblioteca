package castaing.miranda.tl;

import castaing.miranda.bl.*;
import castaing.miranda.dl.CapaLogica;

public class Controller {

    CapaLogica logica = new CapaLogica();

    public Controller() {

    }

    public void procesarPersona(String[] infoPersona) {
        Persona tmpPersona;
        tmpPersona = new Persona(infoPersona[0], infoPersona[1], infoPersona[2]);
        logica.registrarPersona(tmpPersona);
    }

    public String[] listaPersonas(String clase) {
        return logica.getPersonas(clase);
    }

    public void procesarEstudiante(String nombre, String apellido, String id, String carrera, int creditos) {
        Estudiante tmpEstudiante;
        tmpEstudiante = new Estudiante(nombre, apellido, id, carrera, creditos);
        logica.registrarPersona(tmpEstudiante);
    }

    public void procesarProfesor(String nombre, String apellido, String id, char contrato, String fechaContratacion) {
        Profesor tmpProfesor;
        tmpProfesor = new Profesor(nombre, apellido, id, contrato, fechaContratacion);
        logica.registrarPersona(tmpProfesor);
    }

    public void procesarAdministrativo(String nombre, String apellido, String id, char nombramiento, int horas) {
        Administrativo tmpAdministrativo;
        tmpAdministrativo = new Administrativo(nombre, apellido, id, nombramiento, horas);
        logica.registrarPersona(tmpAdministrativo);
    }

    public String carreraEstudiante(String id) {
        return ((Estudiante) logica.buscarPorId(id)).getCarrera();
    }

    public boolean verificarInput(String input) {
        return logica.validarInput(input);
    }

    public boolean verificarPersona(String id) {
        return logica.validarPersona(id);
    }

    public boolean verificarLetras(String texto) {
        return logica.validarLetras(texto);
    }

    public boolean verificarContrato(char contrato) {
        return logica.validarContrato(contrato);
    }

    public boolean verificarFecha(String fecha) {
        return logica.validarFecha(fecha);
    }

    public boolean verificarNombramiento(char nombramiento) {
        return logica.validarNombramiento(nombramiento);
    }

}
