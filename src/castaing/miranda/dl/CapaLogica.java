package castaing.miranda.dl;

import castaing.miranda.bl.*;

import java.time.LocalDate;
import java.util.ArrayList;

public class CapaLogica {

    ArrayList<Persona> personas = new ArrayList<>();
    ArrayList<Material> materiales = new ArrayList<>();
    ArrayList<Texto> textos = new ArrayList<>();
    ArrayList<Audio> audios = new ArrayList<>();
    ArrayList<Video> videos = new ArrayList<>();
    ArrayList<Otro> otros = new ArrayList<>();
    ArrayList<Prestamo> prestamos = new ArrayList<>();

    public void registrarPersona(Persona obj) {
        personas.add(obj);
    }

    public String[] getPersonas(String clase) {
        String[] data;
        int posicion = 0;
        int tamanno = 0;
        for (Persona dato : personas) {
            String info = dato.toString();
            if (info.contains("La información " + clase + " es ") || clase.equals("*")) {
                tamanno++;
            }
        }
        data = new String[tamanno];
        for (Persona dato : personas) {
            String info = dato.toString();
            if (info.contains("La información " + clase + " es ") || clase.equals("*")) {
                data[posicion] = info;
                posicion++;
            }
        }
        return data;
    }

    public Persona buscarPorId(String id) {
        Persona tmpPersona = new Persona();
        for (Persona dato : personas) {
            if (dato.getId().equals(id)) {
                tmpPersona = dato;
                break;
            }
        }
        return tmpPersona;
    }

    public boolean validarInput(String input) {
        return !input.equals("");
    }

    public boolean validarPersona(String id) {
        boolean repetida = false;
        for (Persona dato : personas) {
            if (dato.getId().equals(id)) {
                repetida = true;
                break;
            }
        }
        return repetida;
    }

    public boolean validarLetras(String texto) {
        return texto.matches("^[a-z A-ZáéíóúñÑÁÉÍÓÚüÜ]+$");
    }

    public boolean validarContrato(char contrato) {
        return contrato == 'C' || contrato == 'M';
    }

    public boolean validarNumeros(String numero) {
        return numero.matches("^[0-9]+$");
    }

    public boolean validarPositivo(int numero) {
        return numero > 0;
    }

    public boolean validarFecha(String fecha) {
        boolean correcta = true;
        int slash1 = fecha.indexOf("/");
        if (slash1 != -1 && fecha.indexOf('/', slash1 + 1) != -1) {
            String dia = fecha.split("/")[0];
            String mes = fecha.split("/")[1];
            String stringAnno = fecha.split("/")[2];
            if (validarNumeros(dia) && validarNumeros(mes) && validarNumeros(stringAnno)) {
                int lengthDia = fecha.split("/")[0].length();
                int lengthMes = fecha.split("/")[1].length();
                int anno = Integer.parseInt(stringAnno);
                int annoActual = LocalDate.now().getYear();
                if (!((lengthDia == 1 || lengthDia == 2) && (lengthMes == 1 || lengthMes == 2) &&
                        fecha.split("/")[2].length() == 4 && anno >= annoActual - 125 && anno <= annoActual &&
                        validarPositivo(Integer.parseInt(dia)) && validarPositivo(Integer.parseInt(mes)))) {
                    correcta = false;
                }
            } else {
                correcta = false;
            }
        } else {
            correcta = false;
        }
        return correcta;
    }

    public boolean validarNombramiento(char nombramiento) {
        return nombramiento == 'A' || nombramiento == 'B' || nombramiento == 'C';
    }

}
