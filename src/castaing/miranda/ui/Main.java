package castaing.miranda.ui;

import castaing.miranda.tl.Controller;

import java.io.*;

public class Main {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller gestor = new Controller();

    public static void main(String[] args) throws IOException {
        int opc;
        boolean noSalir;
        do {
            mostrarMenu();
            opc = leerOpcion();
            noSalir = ejecutarAccion(opc);
        } while (noSalir);
    }

    public static void mostrarMenu() {
        out.println("1.  Registrar Persona");
        out.println("2.  Listar Personas");
        out.println("3.  Registrar Estudiante");
        out.println("4.  Listar Estudiantes");
        out.println("5.  Registrar Profesor");
        out.println("6.  Listar Profesores");
        out.println("7.  Registrar Administrativo");
        out.println("8.  Listar Administrativos");
        out.println("9.  Listar Carrera Estudiante");
        out.println("10. Salir");
        out.println();
    }

    public static int leerOpcion() throws java.io.IOException {
        int opcion;
        out.print("Seleccione su opción: ");
        opcion = Integer.parseInt(in.readLine());
        out.println();
        return opcion;
    }

    public static boolean ejecutarAccion(int opcion) throws java.io.IOException {
        boolean noSalirAplicacion = true;
        switch (opcion) {
            case 1:
                registrarPersona(true);
                out.println();
                break;
            case 2:
                listarPersonas();
                break;
            case 3:
                registrarEstudiante();
                break;
            case 4:
                listarEstudiantes();
                break;
            case 5:
                registrarProfesor();
                break;
            case 6:
                listarProfesores();
                break;
            case 7:
                registrarAdministrativo();
                break;
            case 8:
                listarAdministrativos();
                break;
            case 9:
                listarCarreraEstudiante();
                break;
            case 10:
                out.println("Gracias por usar la aplicación");
                noSalirAplicacion = false;
                break;
            default:
                out.println("Opción incorrecta");
                out.println();
                break;
        }
        return noSalirAplicacion;
    }

    public static String[] registrarPersona(boolean unicaPersona) throws IOException {
        String[] infoPersona = new String[3];

        do {
            out.println("Digite el id de la persona");
            infoPersona[2] = in.readLine();
        } while (!gestor.verificarInput(infoPersona[2]));
        if (!gestor.verificarPersona(infoPersona[2])) {
            do {
                out.println("Digite el nombre de la persona");
                infoPersona[0] = in.readLine();
            } while (!gestor.verificarLetras(infoPersona[0]));
            do {
                out.println("Digite el apellido de la persona");
                infoPersona[1] = in.readLine();
            } while (!gestor.verificarLetras(infoPersona[1]));
            if (unicaPersona) {
                gestor.procesarPersona(infoPersona);
            }
        } else {
            out.println();
            out.println("Ya existe una persona con ese id");
        }

        return infoPersona;
    }

    public static void listarPersonas() {
        String[] personas = gestor.listaPersonas("*");
        for (String dato : personas) {
            out.println(dato);
        }
        out.println();
    }

    public static void registrarEstudiante() throws IOException {
        String carrera;
        int creditos;
        String[] infoPersona = registrarPersona(false);

        if (infoPersona[0] != null) {
            do {
                out.println("Digite la carrera en la que está empadronado el estudiante");
                carrera = in.readLine();
            } while (!gestor.verificarLetras(carrera));
            out.println("Digite el número de créditos matriculados en el cuatrimestre actual por el estudiante");
            creditos = Integer.parseInt(in.readLine());
            out.println();
            gestor.procesarEstudiante(infoPersona[0], infoPersona[1], infoPersona[2], carrera, creditos);
        } else {
            out.println();
        }
    }

    public static void listarEstudiantes() {
        String[] estudiantes = gestor.listaPersonas("del estudiante");
        for (String dato : estudiantes) {
            out.println(dato);
        }
        out.println();
    }

    public static void registrarProfesor() throws IOException {
        char contrato;
        String fechaContratacion;
        String[] infoPersona = registrarPersona(false);

        if (infoPersona[0] != null) {
            do {
                out.println("Digite 'C' si el tipo de contrato del profesor es de tiempo completo o 'M' si es de medio " +
                        "tiempo");
                contrato = in.readLine().charAt(0);
            } while (!gestor.verificarContrato(contrato));
            do {
                out.println("Digite la fecha de contratación del profesor (dd/mm/yyyy)");
                fechaContratacion = in.readLine();
            } while (!gestor.verificarFecha(fechaContratacion));
            out.println();
            gestor.procesarProfesor(infoPersona[0], infoPersona[1], infoPersona[2], contrato, fechaContratacion);
        } else {
            out.println();
        }
    }

    public static void listarProfesores() {
        String[] profesores = gestor.listaPersonas("del profesor");
        for (String dato : profesores) {
            out.println(dato);
        }
        out.println();
    }

    public static void registrarAdministrativo() throws IOException {
        char nombramiento;
        int horas;
        String[] infoPersona = registrarPersona(false);

        if (infoPersona[0] != null) {
            do {
                out.println("Digite el tipo de nombramiento ('A', 'B' o 'C') del administrativo");
                nombramiento = in.readLine().charAt(0);
            } while (!gestor.verificarNombramiento(nombramiento));
            out.println("Digite la cantidad de horas semanales asignadas al administrativo");
            horas = Integer.parseInt(in.readLine());
            out.println();
            gestor.procesarAdministrativo(infoPersona[0], infoPersona[1], infoPersona[2], nombramiento, horas);
        } else {
            out.println();
        }
    }

    public static void listarAdministrativos() {
        String[] administrativos = gestor.listaPersonas("del administrativo");
        for (String dato : administrativos) {
            out.println(dato);
        }
        out.println();
    }

    public static void listarCarreraEstudiante() throws IOException {
        String id;

        out.println("Digite el id del estudiante");
        id = in.readLine();
        out.println();
        out.println("El nombre de la carrera es " + gestor.carreraEstudiante(id));
        out.println();
    }

}
